//Creating file

import 'dart:io';
void main() async{
  File f = new File("C2W.txt");
  print(f.runtimeType);  //_File

// Creating file async-1
  print(f.create()); //Instance of 'Future<File>'

// Creating file async-1
  //print(await f.create()); //File: 'C2W.txt'

// Creating file sync : f.createSync();

  print("File Created"); //File Created
}