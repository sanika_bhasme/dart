import 'dart:io';

void main() async{
  File f = new File("C2W.txt");

  print(await f.readAsString());
  /* Using then
  Future<String> str = f.readAsString();
  str.then((value) => print(value));
  */
}