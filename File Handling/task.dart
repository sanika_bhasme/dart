import 'dart:io';

void main() async{
  File file = new File("Employee.txt");

  String? compName = stdin.readLineSync();
  String? empName = stdin.readLineSync();
  int? empId = int.parse(stdin.readLineSync()!);
  String? role = stdin.readLineSync();

  file.writeAsString(" Company name : $compName\n Employee name: $empName\n Employee Id: $empId\n Role:$role"
                      ,mode: FileMode.append);

  // print(await file.readAsString());

  Future<String> data = file.readAsString();
  data.then((value) => print(value));

}