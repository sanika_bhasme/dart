import 'dart:io';

void main() async{
  File f1 = new File("C2W.txt");
  File f2 = new File("ABC.txt");

  //async - 1
  print(f1.copy(await f2.path));

  //async - 2
  Future<File> str = f1.copy("ABC1.txt");
  str.then((value) => print(value));
  
  //f1.copy("xyz.txt");

}