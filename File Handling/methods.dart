import 'dart:io';

void main() async{
  File f = new File("Method.txt");
  f.create();

  print(f.absolute); //File: 'E:\dart\Method.txt'
  print(f.path);     //Method.txt
 /*
  // Methods are asynchornous therefore output is like this(unexpected)
 
  print(f.lastAccessed());  //Instance of 'Future<DateTime>'
  print(f.lastModified());   //Instance of 'Future<DateTime>'
  print(f.length());  //Instance of 'Future<int>'
  print(f.exists());  //Instance of 'Future<bool>'
  */
  
  print(await f.lastAccessed());  //2023-12-16 19:10:32.000
  print(await f.lastModified());   //2023-12-16 19:10:32.000
   
  final len = await f.length(); 
  print(len); //16
 //  print(await f.length());
  /*
  //using then
  final len1 = f.length();
  len1.then((len)=>print(len));*/

  print(await f.exists());  //true
}