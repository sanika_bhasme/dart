abstract class Developer{
  void develop(){
    print("We build Software");
  }
  void devType();
}

class MobileDev implements Developer{
  void develop(){
    print("Flutter Dev");
  }
  //void devType();  //error => Context: 'MobileDev.devType' is defined here.
  void devType(){
    
  }
}

void main(){

}