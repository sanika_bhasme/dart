mixin Parent{
  void m1(){
    print("In m1-parent");
  }
}

mixin Demo{
  void m1(){
    print("In m1-Demo");
  }
}

class Child with Parent,Demo{  //Right most mixin has more priority if same method present in both mixin

}

void main(){
  Child obj = new Child();
  obj.m1();
}

/*Output
In m1-Demo */