mixin Parent{
  void m1(){
    print("In m1-parent");
  }
}

mixin Demo{
  void m2(){
    print("In m2-Demo");
  }
}

class Child with Demo,Parent{

}

void main(){
  Child obj = new Child();
  obj.m1();
  obj.m2();
}

/*Output
In m1-parent
In m2-Demo */