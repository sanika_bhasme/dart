mixin Parent{
  void m1(){
    print("In m1-parent");
  }
  }

class Demo{
  void m1(){
    print("In m1-Demo");

  }
}

class Child extends Demo with Parent{

}

void main(){
  Child obj = new Child();
  obj.m1();
}

/*Output
In m1-parent
*/