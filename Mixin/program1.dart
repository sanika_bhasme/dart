mixin Parent{
  void m1(){
    print("In m1-parent");
  }
}

class Demo{
  void m2(){
    print("In m2-Demo");
  }
}

class Child extends Demo with Parent{

}

void main(){
  Child obj = new Child();
  obj.m1();
  obj.m2();
}