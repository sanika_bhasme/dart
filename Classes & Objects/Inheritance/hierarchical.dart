class Parent{
  int x = 10;
  String str1 = "Name";

  void parentMethod(){
    print(x);
    print(str1);
  }
}

class Child extends Parent{
  int y = 20;
  String str2 = "data";

  void childMethod(){
    print(y);
    print(str2);
  }
}

class Child1 extends Parent{
  int z = 30;
  String str3 = "data1";

  void childMethod1(){
    print(z);
    print(str3);
  }
}

void main(){
  Child1 obj = new Child1();
  obj.parentMethod();
  obj.childMethod1();

  Child obj1 = new Child();
  obj1.parentMethod();
  obj1.childMethod();
}

/*
10
Name
30
data1
10
Name
20
data
*/
