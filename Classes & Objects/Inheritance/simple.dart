class Parent{
  int x = 10;
  String str1 = "Name";

  void parentMethod(){
    print(x);
    print(str1);
  }
}

class Child extends Parent{
  int y = 20;
  String str2 = "data";

  void childMethod(){
    print(y);
    print(str2);
  }
}

void main(){
  Parent obj = new Parent();
  obj.parentMethod();//10 Name
 // obj.childMethod();Error: The method 'childMethod' isn't defined for the class 'Parent'.
}