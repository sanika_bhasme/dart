import 'dart:io';

class Demo{
  final int? x;
  final String? str;
  const Demo(this.x,this.str);
}

void main(){
  Demo obj1 = const Demo(10,"a");
  print(obj1.hashCode);

  Demo obj2 = const Demo(10,"a");
  print(obj2.hashCode);

  Demo obj3 = const Demo(20,"b");
  print(obj3.hashCode);
}

/*
735683742
735683742
909697914
*/