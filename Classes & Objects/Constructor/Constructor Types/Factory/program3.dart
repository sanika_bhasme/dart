class Demo{
  Demo._private(){
    print("In private constructor");
  }

  factory Demo(){
    print("In Factory constructor");
    return Demo._private();
  }

  void fun(){
    print("In fun");
  }
}
