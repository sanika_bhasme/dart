class Demo{
  
  static Demo obj = new Demo(); // In Constructor
  int x = 10;
  Demo(){
    print("In Constructor");
  }
}

void main(){
  Demo obj1 = new Demo(); // In Constructor
  print(Demo.obj);  // Instance of 'Demo'
}

/* Output
In Constructor
In Constructor
Instance of 'Demo'
*/