abstract class Developer{
  factory Developer(String devType){
    if(devType == "Backend")
      return Backend();
    else if(devType == "Frontend")
      return Frontend();    
    else if(devType == "Mobile")
      return Mobile();
    else 
      return Other();
  }

  void devLang();
}

class Backend implements Developer{
  void devLang(){
    print("NodeJS");
  }
}
class Frontend implements Developer{
  void devLang(){
    print("AngularJS/ReactJS");
  }
}
class Mobile implements Developer{
  void devLang(){
    print("Flutter/Android");
  }
}
class Other implements Developer{
  void devLang(){
    print("DevOps/Testing/Support");
  }
}

void main(){
  Developer obj1 = new Developer("Frontend");
  obj1.devLang();
  Developer obj2 = new Developer("Backend");
  obj2.devLang();
  Developer obj3 = new Developer("Mobile");
  obj3.devLang();
  Developer obj4 = new Developer("Other");
  obj4.devLang();
}