class Demo{
  // Demo obj = new Demo();    // Unhandled exception:Stack Overflow
  
  static Demo obj = new Demo(); // obj is not created because it is not initialize

  Demo(){
    print("In Constructor");
  }

}

void main(){
  Demo obj1 = new Demo(); // In COnstructor
}
