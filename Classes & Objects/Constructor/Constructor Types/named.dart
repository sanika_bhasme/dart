class Demo{
  int? emp;
  String? compName;
  
  Demo(){
    print("Default");
  }
  Demo.constr(this.emp,this.compName);
  void info(){
    print(emp);
    print(compName);
  }
}

void main(){
  Demo obj1 = new Demo();
  Demo obj = new Demo.constr(100, "A");
  obj.info();
}

/*
Default
100
A*/