class Demo{
  int? x;
  String? str;
  Demo(int x, String str){
    print("In Parameterized Constructor");
    this.x = x;
    this.str = str;
  }

  void printdata(){
    print(x);
    print(str);
  }
}

void main(){
  Demo obj = new Demo(10,"kanha");
  obj.printdata();
}

/*output
In Parameterized Constructor
10
kanha */