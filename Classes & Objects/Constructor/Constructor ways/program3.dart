//Default parameter

class Demo{
  int? emp;
  String? compName;
  
  Demo(this.emp,{this.compName="xyz"});
  void info(){
    print(emp);
    print(compName);
  }
}

void main(){
 /* Demo obj1 = new Demo(100, "A");
Error: Too many positional arguments: 1 allowed, but 2 found.
Try removing the extra positional arguments.
  Demo obj1 = new Demo(100, "A");
                      ^*/
  Demo obj2 = new Demo(200);
 
 // obj1.info();
  obj2.info(); //200  xyz
}