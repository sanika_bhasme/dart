//optional parameter

class Demo{
  int? emp;
  String? compName;
  
  Demo(this.emp,[this.compName="xyz"]);
  void info(){
    print(emp);
    print(compName);
  }
}

void main(){
  Demo obj1 = new Demo(100, "A");
  Demo obj2 = new Demo(200);
  obj1.info();
  obj2.info();
}
/*
100
A
200
xyz*/