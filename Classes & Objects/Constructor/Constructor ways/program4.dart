//Named parameter

class Demo{
  int? emp;
  String? compName;
  
  Demo({this.emp,this.compName});
  void info(){
    print(emp);
    print(compName);
  }
}

void main(){
  Demo obj1 = new Demo(emp:100,compName: "A");
  Demo obj2 = new Demo(compName:"xyz",emp: 567);
  obj1.info();//100 A
  obj2.info();//567 xyz
  
}