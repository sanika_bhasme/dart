class Demo{
  int? x;
  String? str;
  Demo(int value,String name){
    print("In parameterized constructor");
  }
  void printdata(){
    print(x);
    print(str);
  }
}

void main(){
  Demo obj = new Demo(10,"Kanha");
  obj.printdata(); 
}
/*
In parameterized constructor
null
null
*/