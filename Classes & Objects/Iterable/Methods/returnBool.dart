void main(){
  var Players = ["Rohit","Virat","Shubhman","KLRahul","Shreyas","Hardik"];

  
  //skipWhile(bool Function(String) test)
  var ret = Players.skipWhile((player) => player[0]!="K");
  print(ret);
}

/*
dry run:
R != K    // true
V != K    // true
S != K    // true  ----- skip
K != K    // false
*/
//Output:(KLRahul, Shreyas, Hardik)