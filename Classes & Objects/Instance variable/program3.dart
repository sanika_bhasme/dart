class Demo{
  int x = 10;
  static int y = 20;
  void printData(){
    print(x);
    print(y);
  }
}

void main(){
  Demo obj1 = new Demo();
  obj1.printData();
  Demo obj2 = new Demo();
  obj2.printData();
  obj1.x = 100;
  // obj1.y = 200; Error:"The static setter 'y' can't be accessed through an instance.\nTry using the class 'Demo' to access the setter."

  obj1.printData();
  obj2.printData();
}

/*Output:
10
20
10
20
100
20
10
20 */