class Company{
  int empCount = 500;
  String compName = "Google";
  String loc = "Pune";
  void compInfo(){
    print(compName);
    print(empCount);
    print(loc);
  }
}

void main(){
  Company obj1 = new Company();
  Company obj2 = Company();
  new Company().compInfo(); //only one method can call 
  Company().compInfo(); //only one method can call 

  obj1.compInfo();
  obj2.compInfo();
}

/* Output:
Google
500
Pune
Google
500
Pune
Google
500
Pune
Google
500
Pune */