class Demo{
  int? x;
  String? str;
  void printdata(){
    print(x);
    print(str);
  }
}

void main(){
  Demo obj = new Demo();
  obj.printdata(); //null null

  obj.x = 10;
  obj.str = "Kanha";

  obj.printdata();// 10 kanha
}