//Sum of num from 1-10;
int sum = 0;

void fun(int num){
  if(num>10){
    return;
  }

  sum = sum +num;
  num++;
  fun(num);
}

void main(){
  fun(1);
  print(sum);
}