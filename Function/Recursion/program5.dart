// Factorial of num

int fact = 1;

void fun(int num){
  if(num>5){
    return;
  }

  fact = fact *num;
  num++;
  fun(num);
}

void main(){
  fun(1);
  print(fact);
}