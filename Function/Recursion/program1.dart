// 1 - 10 print by recursion


void fun(int x){
  if(x>10){
    return;
  }
  print(x);
  x++;
  fun(x);
}
void main(){
  fun(1);
}