void main(){
  int x = 10;
  double y = 20.5;
  num z = 30;

  print(x is double);
  print(y is int);
  print(z is! int);
  print(x is num);
  print(x is double);
}